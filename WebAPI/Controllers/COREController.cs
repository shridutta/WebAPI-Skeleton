﻿using AttributeRouting;
using AttributeRouting.Web.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using AttributeRouting.Web.Http.WebHost;
using WebAPI.Filters;
using WebAPI.Utilities;
using System.Web.Script.Serialization;
using WebAPI.Models;
using System.Configuration;
using AuthProvider.Repository;
using AuthProvider.Models;
using System.IO;
using AuthProvider.Helpers;
using WebAPI.API.Utilities;

namespace WebAPI.Controllers
{
    /// <summary>
    /// Controller Class that implements the Operation that coming from mobile client
    /// <summary>
    [WebAPIVersion]
    [WebAPIAuthorize]    
    public class COREController : ApiController
    {
        private HttpResponseMessage _response;
        private TokenHelper _tokenHelper;
        private ServiceHelper _serviceHelper;
       
        /// <summary>
        /// Initilization for Authtoken, A Service and AuthManager object using Constructor
        /// </summary>
        public COREController()
        {
            _response = new HttpResponseMessage();
            _tokenHelper = new TokenHelper();
            //_someRepository = new SOMERepository();
        }
        
        /// <summary>
        /// This method is used to Authenticate the User
        /// </summary>
        //[GET(version+"core/authtoken"), HttpGet]
        [HttpGet]
        [HttpRoute("/core/authtoken")]
        public HttpResponseMessage Authenticate()
        {
          try
            {
                _serviceHelper = new ServiceHelper(Request);
                var authenticateRequestModel = new AuthenticateRequestModel();
                authenticateRequestModel.username = _serviceHelper.GetKeyValue("WebAPI-Username");
                authenticateRequestModel.password = _serviceHelper.GetKeyValue("WebAPI-Password");
                if (!string.IsNullOrEmpty(authenticateRequestModel.username) && !string.IsNullOrEmpty(authenticateRequestModel.password))
                {
                        //Call the SOME Service to check the user authenticate or not
                        //If service returns true
                        Token authtoken = _tokenHelper.GenerateAuthToken(authenticateRequestModel.username, DateTime.Now.AddHours(Convert.ToDouble(ConfigurationManager.AppSettings["TokenExpiryHours"])));
                        _tokenHelper.AddToken(authtoken);
                         _response = new WebAPIResponseMessage("200", HttpStatusCode.OK);
                        _response.Headers.Add("WebAPI-AuthToken", "Bearer " + authtoken.TokenString);
                    }
                    else
                    _response = new WebAPIResponseMessage(new ErrorResponseMessage("User is not authenticated"), "application/json", "401", HttpStatusCode.Unauthorized);
            }
            catch (Exception ex)
            {
                ErrorLogger(ex);
                _response = new WebAPIResponseMessage("500", HttpStatusCode.InternalServerError);
            }
            finally
            {
                //_serviceHelper = null;
            }
            return _response;
        }

        private Token GetAuthToken()
        {
            IAuthManagerRepository authRepo = new AuthManagerRepository();
            return authRepo.GetAuthToken(Convert.ToString(_serviceHelper.GetKeyValue("Authorization")));
        }

        private void ErrorLogger(Exception ex)
        {
            //string filePath = ConfigurationManager.AppSettings["LogFilePath"];
            //using (StreamWriter writer = new StreamWriter(filePath, true))
            //{
            //    writer.WriteLine("Message :" + ex.Message + "<br/>" + Environment.NewLine + "StackTrace :" + ex.StackTrace +
            //       "" + Environment.NewLine + "Date :" + DateTime.Now.ToString());
            //    writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
            //}
        }
    }
}