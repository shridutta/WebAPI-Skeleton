﻿using WebAPI.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using WebAPI.API.Utilities;

namespace WebAPI.Filters
{
    public class WebAPIVersionAttribute : ActionFilterAttribute, IActionFilter
    {
        public override void OnActionExecuting(HttpActionContext context)
        {
            if (context.Request.RequestUri.AbsolutePath.Split('/')[1] != ConfigurationManager.AppSettings["version"]);
                // ReturnNotFound(context); 
        }

        /// <summary>
        /// This method will return the Bad Request
        /// </summary>
        /// <param name="context"></param>
        private void ReturnNotFound(System.Web.Http.Controllers.HttpActionContext context)
        {
            context.Response = new WebAPIResponseMessage("404", HttpStatusCode.NotFound);
        }
    }
}