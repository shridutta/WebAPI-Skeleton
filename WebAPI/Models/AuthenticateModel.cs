﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using WebAPI.SOMEServices;
using System.Runtime.Serialization;

namespace WebAPI.Models
{
    public class AuthenticateRequestModel : AuthenticateRequest
    {
       
    }

    public class AuthenticateResponseModel : AuthenticateResponse
    {

    }

    public class AuthenticateRequest 
    {
        private string passwordField;
        private string usernameField;

        public string password { get; set; }
        public string username { get; set; }
    }

    public class AuthenticateResponse : GenericResponse
    {
        private bool SuccessField;
        public bool Success { get; set; }
        public int UserId { get; set; }
    }

    public class GenericResponse 
    {
        private string ErrorMsgField { get; set; }
        public string ErrorMsg { get; set; }
    }
    
}