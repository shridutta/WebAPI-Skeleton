﻿//using WebAPI.Filters;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http;

namespace WebAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
              name: "CORERoutes",
              routeTemplate: ConfigurationManager.AppSettings["version"] + "/core/{action}",
              defaults: new { controller = "CORE" }
             );

            config.Routes.MapHttpRoute(
              name: "APIRoutes",
              routeTemplate: "API/{action}",
              defaults: new { controller = "ValuesController" }
             );

            config.Routes.MapHttpRoute(
               name: "COREUserRoutes",
               routeTemplate: ConfigurationManager.AppSettings["version"] + "/core/{action}",
               defaults: new { controller = "COREUser" }
              );

            config.Routes.MapHttpRoute(
               name: "Routes",
               routeTemplate: ConfigurationManager.AppSettings["version"] + "/PATH/{action}",
               defaults: new { controller = "PATH" }
           );

            config.Routes.MapHttpRoute(
                "GetById",
                "Path/{Id}",
                new { controller = "ChequeController", action = "GetChequeById" },
                new { chequeId = @"\d+" });

            config.Routes.MapHttpRoute(
                "GetByStatus",
                "Path/{status}",
                new { controller = "ChequeController", action = "GetChequesByStatus" });

        }


    }
}
