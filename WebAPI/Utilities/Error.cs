﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Utilities
{
    public class ErrorResponseMessage
    {
        public ErrorResponseMessage(string message)
        {
            this.message = message;
        }

        public string message { get; set; }
    }
}