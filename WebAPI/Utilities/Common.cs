﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.IO;
using System.Web.Script.Serialization;

namespace WebAPI.Utilities
{
    /// <summary>
    /// Common Class that implements the Operation related to datatype conversion
    /// <summary>
    public class Common
    {
        /// <summary>
        /// This method is used to split the string data according to splitter
        /// </summary>
        public static string[] SplitText(string Data, string Splitter)
        {
            Data = Data.Replace("\r", string.Empty);
            return Data.Split(new string[] { Splitter }, StringSplitOptions.RemoveEmptyEntries);
        }

        /// <summary>
        /// This method is used to Covert Object to string
        /// </summary>
        public static string ConvertString(object value)
        {
            string temp = Convert.ToString(value);
            if (temp == string.Empty)
                return null;
            else
                return temp;
        }

        /// <summary>
        /// This method is used to Covert Object to int
        /// </summary>
        public static int? ConvertInt(object value)
        {
            string temp = Convert.ToString(value);
            if (temp == string.Empty)
                return null;
            else
                return Convert.ToInt32(temp);
        }

        /// <summary>
        /// This method is used to check null integer
        /// </summary>
        public static int ValidateInteger(int value)
        {
            int? num = value;
            int result = num ?? 0;

            return result;
        }

        /// <summary>
        /// This method is used to Covert Object to decimal
        /// </summary>
        public static decimal? ConvertDecimal(object value)
        {
            string temp = Convert.ToString(value);
            if (temp == string.Empty)
                return null;
            else
                return Convert.ToDecimal(temp);
        }

        /// <summary>
        /// This method is used to Covert Object to bool
        /// </summary>
        public static bool? ConvertBool(object value)
        {
            string temp = Convert.ToString(value);
            if (temp == string.Empty)
                return null;
            else
                return Convert.ToBoolean(temp);
        }

        /// <summary>
        /// This method is used to Covert string to bitmap
        /// </summary>
        public static Bitmap ConvertBase64ToBitMap(string inputData)
        {
            Byte[] bitmapData = new Byte[inputData.Length];
            bitmapData = Convert.FromBase64String(inputData);
            using (MemoryStream streamBitmap = new MemoryStream(bitmapData))
            {
                Bitmap bitmap = new Bitmap((Bitmap)Image.FromStream(streamBitmap));
                return bitmap;
            }
        }

        /// <summary>
        /// This method is used to Covert bitmap to base64string
        /// </summary>
        public static string ConvertBitMapToBase64(Bitmap bitmap)
        {
            if (bitmap == null)
            {
                return null;
            }
            else
            {
                string base64String = string.Empty;
                System.IO.MemoryStream stream = new System.IO.MemoryStream();
                bitmap.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
                byte[] imageBytes = stream.ToArray();

                return base64String = Convert.ToBase64String(imageBytes);
            }
        }

        /// <summary>
        /// This method is performed the deserialization
        /// </summary>
        public static object ConvertJsonToObject(string inputData)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            return jsSerializer.DeserializeObject(inputData);
        }

        /// <summary>
        /// This method is used to Object to JSON
        /// </summary>
        public static string ConvertObjectToJson(Object value)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            string message = js.Serialize(value);

            return message;
        }

        /// <summary>
        /// This method is used to convert string to JSON
        /// </summary>
        public static string ConvertStringToJson(string inputData)
        {
            //return JObject.Parse(inputData);
            object test = inputData = (object)"{" + inputData + "}";
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(test);
        }


        /// <summary>
        /// This method is used to convert Dictionary to JSON
        /// </summary>
        public static string MyDictionaryToJson(Dictionary<int, List<int>> dict)
        {
            var entries = dict.Select(d =>
                string.Format("\"{0}\": [{1}]", d.Key, string.Join(",", d.Value)));
            return "{" + string.Join(",", entries) + "}";
        }

        /// <summary>
        /// This method is used to count the payload multipart boundary in payloaddata
        /// </summary>
        public static int PayloadBoundaryCount(string payloadData, string payloadBoundary)
        {
            int count = 0;
            int i = 0;
            while ((i = payloadData.IndexOf(payloadBoundary, i)) != -1)
            {
                i += payloadBoundary.Length;
                count++;
            }
            return count;
        }

    }
}