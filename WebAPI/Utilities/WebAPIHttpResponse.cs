﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Net.Http.Formatting;
using WebAPI.Utilities;


namespace WebAPI.API.Utilities
{
    public class WebAPIResponseMessage : HttpResponseMessage
    {
        //private HttpResponse _response;
        public WebAPIResponseMessage()
        {
            this.Headers.Add("Language", "English");
        }

        public WebAPIResponseMessage(String message, string contentType, string code, HttpStatusCode status)
        {
            // this.Content = new StringContent(message,null,contentType);
            this.Content = new StringContent(message, Encoding.UTF8, contentType);
            this.Headers.Add("Language", "English");
            //this.Headers.Add("Code", code);
            //this.Headers.Add("Description", Regex.Replace(status.ToString(), "([a-z])([A-Z])", "$1 $2"));
            this.StatusCode = status;
        }

        public WebAPIResponseMessage(object message, string contentType, string code, HttpStatusCode status)
        {
            this.Content = new StringContent(Common.ConvertObjectToJson(message), Encoding.UTF8, contentType);
            this.Headers.Add("Language", "English");
            //this.Headers.Add("Code", code);
            //this.Headers.Add("Description", Regex.Replace(status.ToString(), "([a-z])([A-Z])", "$1 $2"));
            this.StatusCode = status;
        }



        public WebAPIResponseMessage(string code, HttpStatusCode status)
        {
            this.Headers.Add("Language", "English");
            //this.Headers.Add("Code", code);
            //this.Headers.Add("Description", Regex.Replace(status.ToString(), "([a-z])([A-Z])", "$1 $2"));
            this.StatusCode = status;
        }

    }
}