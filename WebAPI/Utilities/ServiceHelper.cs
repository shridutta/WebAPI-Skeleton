﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;
using System.Web.Script.Serialization;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Threading.Tasks;

namespace WebAPI.Utilities
{
    /// <summary>
    /// ServiceHelper Class that implements the Operation of Request Response of API
    /// <summary>
    public class ServiceHelper
    {
        public HttpRequestMessage _request = null;
        public HttpResponseMessage _response = null;

        ///// <summary>
        ///// Initilization for ServiceHelper default Constructor
        ///// </summary>
        public ServiceHelper()
        {
        }

        ///// <summary>
        ///// Initilization for HttpRequestMessage reference using Constructor
        ///// </summary>
        public ServiceHelper(HttpRequestMessage Request)
        {
            this._request = Request;
            _response = new HttpResponseMessage();
        }

        public int GetCustomerId()
        {
            //int customerId = Convert.ToInt32(HttpContext.Current.Session["CustomerId"]);
            int customerId = 77;
            return customerId;
        }

        /// <summary>
        /// Defines the GetRequestHeader Method that capture the key value from the request
        /// </summary>
        /// <param>GetRequestHeader Request data i.e key and value coming with header in request</param>
        /// <returns>GetRequestHeader Response returns the key value pair</returns>
        public Dictionary<string, IEnumerable<string>> GetRequestHeaders()
        {
            string HeaderName = string.Empty;
            string HeaderValue = string.Empty;
            var _requestHeaders = _request.Headers;

            var queryStrings = _request.Headers;
            var match = queryStrings.ToDictionary(kv => kv.Key, kv => kv.Value, StringComparer.OrdinalIgnoreCase);

            return match;      
        }

        /// <summary>
        /// Defines the GetRequestHeader Method that performs the validation of Key
        /// </summary>
        /// <param>GetRequestHeader Request data i.e Key coming from Controller</param>
        /// <returns>GetRequestHeader Response returns the value with proper Key</returns>
        public string GetKeyValue(string key)
        {
            Dictionary<string, IEnumerable<string>> diHeaders = GetRequestHeaders();
            IEnumerable<string> keys = null;
            if (!diHeaders.TryGetValue(key, out keys))
                return null;

            string value = keys.First();

            return value;
        }

    
        string[] MetaDataKeys = new string[] { "Content-Type", "Content-Transfer-Encoding" };//"Content-Disposition",

        public Dictionary<string, string> ParseFormData(string[] PayLoadArray)
        {
            Dictionary<string, IEnumerable<string>> diHeaders = GetRequestHeaders();
            Dictionary<string, string> diPayLoad = new Dictionary<string, string>();
            try
            {
                if (diHeaders.Keys.Contains("Boundary"))
                {
                    string Boundary = GetKeyValue("Boundary");

                    foreach (string PayLoad in PayLoadArray)
                    {
                        StringBuilder sb = new StringBuilder();

                        string Name = string.Empty;
                        string[] array = Common.SplitText(PayLoad, "\n");//PayLoad.Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);

                        for (int i = 0; i < array.Length; i++)// temp in array)
                        {
                            string temp = array[i];

                            if (temp.Contains(":"))//Check if it is a MetaDataKey like Content-Disposition etc
                            {
                                string MetaDataKey = temp.Split(':')[0].Trim();
                                if (MetaDataKey == "Content-Disposition")
                                {
                                    if (temp.Contains("name"))
                                    {
                                        string[] NameArray = Common.SplitText(temp, "=");//temp.Split(new string[] { "name" })[0].Trim();
                                        if (NameArray.Length == 2)
                                            Name = NameArray[1];
                                        else
                                        {
                                            Name = NameArray[2];
                                        }

                                        continue;
                                    }
                                    else
                                    {
                                        throw new Exception(string.Format("PayLoad {0} doesnot contain name", i));
                                    }
                                }
                                else if (MetaDataKeys.Contains(MetaDataKey))
                                {
                                    continue;
                                }
                                //else
                                //{
                                //        throw new Exception(string.Format("PayLoad {0} doesnot contain Content-Disposition", i));
                                //}
                            }
                            // otherwise it's data
                            sb.Append(temp.Trim());
                        }
                        diPayLoad.Add(Name, sb.ToString());
                    }
                }
                else
                {
                    throw new Exception("Header missing : Boundary");
                }
        }
        catch(Exception ex)
        {
            return null;
        }

            return diPayLoad;
        }

       
        public HttpResponseMessage HeaderErrorMessage()
        {
            _response.Headers.Add("Code", "400");
            _response.Headers.Add("Description", HttpStatusCode.BadRequest.ToString());
            _response.StatusCode = HttpStatusCode.BadRequest;

            return _response;
        }

        public HttpResponseMessage PayloadErrorMessage(string errorMessage)
        {
             string responseErrorMessage = Common.ConvertStringToJson(errorMessage);
            _response.Content = new StringContent(responseErrorMessage, Encoding.UTF8, "application/json");
            _response.Headers.Add("Code", "400");
            _response.Headers.Add("Description", HttpStatusCode.BadRequest.ToString());
            _response.StatusCode = HttpStatusCode.BadRequest;

            return _response;
        }

        /// <summary>
        /// This method is return yhe authentication sucess message
        /// </summary>
        public  HttpResponseMessage SucessHeaderResponse()
        {
            HttpResponseMessage response = new HttpResponseMessage();
            response.Headers.Add("Code", "201");
            response.Headers.Add("Description", HttpStatusCode.Created.ToString());
            response.StatusCode = HttpStatusCode.Created;

            return response;
        }

        public HttpResponseMessage FailureHeaderResponse()
        {
            HttpResponseMessage response = new HttpResponseMessage();
            response.Headers.Add("Code", "500");
            response.Headers.Add("Description", HttpStatusCode.InternalServerError.ToString());
            response.StatusCode = HttpStatusCode.InternalServerError;

            return response;
        }

        public HttpResponseMessage FailureHeaderResponse(string exception)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            response.Headers.Add("Code", "500");
            response.Headers.Add("Description", exception);
            response.StatusCode = HttpStatusCode.InternalServerError;

            return response;
        }

        //public async Task<string> GetRequestPayLoad()
        //{
        //    string Data = string.Empty;
        //    Data = await _request.Content.ReadAsStringAsync();

        //    //HttpContent requestContent = _request.Content;
        //    //string Data = requestContent.ToString();

        //    return Data;
        //}
    }
}