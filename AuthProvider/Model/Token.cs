﻿using AuthProvider.Repository;
using JWT;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace AuthProvider.Models
{
    public class Token
    {
        private string _token;

        public Guid Id { get; set; }
        public string UserName {get;set;} //Not Needed
        public DateTime Expires { get; set; }
        //public string Subject { get; set; } //Not Needed
        //public DateTime Issued { get; set; } //Not needed
        public IDictionary<string, string> Claims { get; set; }
      
        //private const string Issuer = "https://API/v1/core/authtoken";

        public Token()
        { }

        public Token(string usernm, DateTime exp)
        {
            Id = new Guid();
            UserName = usernm; // Not Needed
            //Subject = sub; //Not Needed
            //Issued = issue; //Not Needed
            Expires = exp;
            Claims = new Dictionary<string, string>();
            Claims.Add("Guid", Convert.ToString(Id));
            Claims.Add("Expires", exp.ToString());
        }

        public string TokenString
        {
            get
            {
                if (_token == null) throw new InvalidOperationException("Issued token not encoded");
                return _token;
            }
            set
            {
                _token = value;
            }
        }
    }
}