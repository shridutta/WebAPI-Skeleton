﻿using JWT;
using System;
using System.Collections.Generic;

namespace AuthProvider.Models
{
    public sealed class TokenManager
    {
        private static volatile TokenManager instance;
        private static object syncRoot = new Object();
        public IDictionary<string, Token> TokenList;

        private TokenManager() { TokenList = new Dictionary<string, Token>(); }
        public static TokenManager TokenProvider
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                        {
                            instance = new TokenManager();
                        }
                    }
                }

                return instance;
            }
        }

    }
}