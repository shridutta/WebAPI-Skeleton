﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace AuthProvider.Models
{
    public class AuthResponseMessage : HttpResponseMessage
    {
        public AuthResponseMessage()
        {
            this.Headers.Add("Language", "English");
        }

        public AuthResponseMessage(String message, string contentType, string code, HttpStatusCode status)
        {
            this.Content = new StringContent(message, Encoding.UTF8, contentType);
            this.Headers.Add("Language", "English");
            //this.Headers.Add("Code", code);
            //this.Headers.Add("Description", Regex.Replace(status.ToString(), "([a-z])([A-Z])", "$1 $2"));
            this.StatusCode = status;
        }

        public AuthResponseMessage(object message, string contentType, string code, HttpStatusCode status)
        {
            this.Content = new StringContent(ConvertObjectToJson(message), Encoding.UTF8, contentType);
            this.Headers.Add("Language", "English");
            //this.Headers.Add("Code", code);
            //this.Headers.Add("Description", Regex.Replace(status.ToString(), "([a-z])([A-Z])", "$1 $2"));
            this.StatusCode = status;
        }

        public AuthResponseMessage(string code, HttpStatusCode status)
        {
            this.Headers.Add("Language", "English");
            //this.Headers.Add("Code", code);
            //this.Headers.Add("Description", Regex.Replace(status.ToString(), "([a-z])([A-Z])", "$1 $2"));
            this.StatusCode = status;
        }

        public static string ConvertObjectToJson(Object value)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            string message = js.Serialize(value);

            return message;
        }
    }
}
