﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthProvider.Models
{
    public class AuthErrorResponseMessage
    {
        public AuthErrorResponseMessage(string message)
        {
            this.message = message;
        }

        public string message { get; set; }
    }
}
