﻿using AuthProvider.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthProvider.Repository
{
    public interface IAuthManagerRepository
    {
        void AddAuthToken(string tokenString, Token token);
        void RemoveAuthToken(string tokenString);
        IDictionary<string, Token> GetAllTokens();
        Token GetAuthToken(string tokenString);
        bool CheckAuthToken(string tokenString);
    }
}
