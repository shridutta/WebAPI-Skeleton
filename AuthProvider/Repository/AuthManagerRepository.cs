﻿using AuthProvider.ExceptionHandler;
using AuthProvider.Helpers;
using AuthProvider.Models;
using AuthProvider.Repository;
using JWT;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AuthProvider.Repository
{
    public class AuthManagerRepository : IAuthManagerRepository
    {
        public void AddAuthToken(string tokenString, Token token)
        {
            TokenManager.TokenProvider.TokenList.Add("Bearer " + tokenString, token);
        }

        public void RemoveAuthToken(string tokenString)
        {
            TokenManager.TokenProvider.TokenList.Remove(tokenString);
        }

        public IDictionary<string, Token> GetAllTokens()
        {
            return TokenManager.TokenProvider.TokenList;
        }

        public Token GetAuthToken(string tokenString)
        {
            return TokenManager.TokenProvider.TokenList[tokenString];
        }

        public bool CheckAuthToken(string tokenString)
        {
            //Verify the calims are present in the tokenString - If 4 claims (UserName, Expires are not present then throw error else
            TokenHelper tokenHelper = new TokenHelper();
            IDictionary<string, object> claimsRaw = tokenHelper.ReadClaims(tokenString);
            if (claimsRaw == null)
                return false; // throw new TokenDecodeException("Token has no claims");
            else if (String.IsNullOrEmpty(claimsRaw["UserName"].ToString()) || String.IsNullOrEmpty(claimsRaw["Expires"].ToString()))
                return false;// throw new TokenDecodeException("Invalid Token");
            //Try to regenerate **tokenString**  using claims - If generated token is same as received - It is a valid token else if they do not match throw error
            Token token = tokenHelper.GenerateAuthToken(claimsRaw["UserName"].ToString(), Convert.ToDateTime(claimsRaw["Expires"]));
            if ("Bearer " + token.TokenString == tokenString)//&& !TokenManager.TokenProvider.TokenList.ContainsKey(tokenString))
            {
                if (!TokenManager.TokenProvider.TokenList.ContainsKey(tokenString))
                    tokenHelper.AddToken(token);
                return true;
            }
            else
                return false;
        }

    }
}