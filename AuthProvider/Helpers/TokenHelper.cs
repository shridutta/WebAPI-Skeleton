﻿using AuthProvider.ExceptionHandler;
using AuthProvider.Models;
using AuthProvider.Repository;
using JWT;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace AuthProvider.Helpers
{
    public class TokenHelper
    {
        private const string EncryptionKey = "GQDstcKsx0NHjPOuXOYg5MbeJ1XT0uFiwDVvVBrk";
        private static readonly DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public TokenHelper() { }
        public IDictionary<string, object> ReadClaims(string tokenString)
        {
            return ((IDictionary<string, object>)JsonWebToken.DecodeToObject(tokenString.Replace("Bearer", "").Trim(), EncryptionKey));
        }
        public Token Decode(Token t)
        {
            try
            {

                t.TokenString = JsonWebToken.Decode(t.TokenString, EncryptionKey);
                return t;
            }
            catch (SignatureVerificationException)
            {
                //Log.Warning("Token validation failed: decrypt {token}", token);
                throw new TokenDecodeException("Signature verification failed");
            }
            catch (Exception exception)
            {
                //Log.Warning(exception, "Token validation failed: {token} {errorMessage}", token, exception.Message);
                throw new TokenDecodeException(exception.Message);
            }
        }

        public Token Encode(Token t)
        {
            t.TokenString = JsonWebToken.Encode(t.Claims, EncryptionKey, JwtHashAlgorithm.HS256); ;
            return t;
        }

        /// <summary>
        /// Defines the Generate AuthToken Method that performs to generate a AuthToken 
        /// </summary>
        /// <param>Generate AuthToken Request data null coming from CCSMobile.svc.cs</param>
        /// <returns>Generate AuthToken Response a Auth Token</returns>
        public Token GenerateAuthToken(string userName, DateTime expiryTime)
        {
            IAuthManagerRepository authRepo = new AuthManagerRepository();
            Token token = new Token(userName,expiryTime);
            Token receivedToken = this.Encode(token);
            //authRepo.AddAuthToken(receivedToken.TokenString, token);
            return receivedToken;
        }

        public bool AddToken(Token token)
        {
            bool retVal = false;
            IAuthManagerRepository authRepo = new AuthManagerRepository();
            authRepo.AddAuthToken(token.TokenString, token);
            retVal = true;
            return retVal;
        }

        private DateTime FromUnixTime(string unixTime)
        {
            var secs = long.Parse(unixTime);
            return Epoch.AddSeconds(secs);
        }

        private string ToUnixTime(DateTime date)
        {
            return Convert.ToInt64((date - Epoch).TotalSeconds).ToString();
        }
    }
}
