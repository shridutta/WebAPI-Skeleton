﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthProvider.ExceptionHandler
{
    public class TokenDecodeException : Exception
    {
        public TokenDecodeException(string message)
            : base(message)
        { }
    }
}
