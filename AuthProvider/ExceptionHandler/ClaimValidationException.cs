﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthProvider.ExceptionHandler
{
    class ClaimValidationException : Exception
    {
        public ClaimValidationException(string message):base(message)
        { }
    }
}

