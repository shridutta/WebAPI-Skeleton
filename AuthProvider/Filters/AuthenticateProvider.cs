﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Net.Http;
using System.Web.Http.Filters;
using System.Threading;
using System.Security.Principal;
using System.Text;
using AuthProvider.Models;
using System.Configuration;
using AuthProvider.Repository;
using System.Web.Script.Serialization;
using AuthProvider.ExceptionHandler;
using System.Web.Http.Controllers;

namespace AuthProvider.Filters
{
    public class AuthenticateProvider : AuthorizationFilterAttribute
    {
        bool _isAuthenticated = true;
        public bool IsAuthenticated { get; set; }
        public override void OnAuthorization(HttpActionContext context)
        {
            // Get the AuthKey and 
            var authKey = context.Request.Headers.Contains("ApiKey") ? Convert.ToString(((string[])(context.Request.Headers.GetValues("ApiKey")))[0]) : String.Empty;
            string tokenString = context.Request.Headers.Contains("Authorization") ? Convert.ToString(((string[])(context.Request.Headers.GetValues("Authorization")))[0]) : String.Empty;
            string apiKey = ConfigurationManager.AppSettings["APIKEY"];

            if (String.IsNullOrEmpty(authKey))
                ReturnUnAthorized(context);
            else if (!CheckAuthKey(authKey))
                ReturnUnAthorized(context);
            else if (String.IsNullOrEmpty(tokenString))
                ReturnUnAuthenticated(context);
            else
            {
                try
                {
                    IAuthManagerRepository authRepo = new AuthManagerRepository();

                    if (!authRepo.CheckAuthToken(tokenString.Trim()))
                        ReturnUnAuthenticated(context);
                    else if (!apiKey.Equals(Convert.ToString(authKey)))
                        ReturnUnAthorized(context);
                    else
                    {
                        Token authToken = authRepo.GetAuthToken(tokenString);
                        if (authToken != null)
                        {
                            DateTime date = DateTime.Now;
                            if ((authToken.Expires - date).TotalHours <= 0)
                            {
                                ReturnUnAuthenticated(context);
                                CleanUpAuthTokens(date, authRepo);
                            }
                        }
                        else
                            ReturnUnAuthenticated(context);

                        if (context.Response == null)
                        {
                            if (context.Request.Method.Method.ToUpper().Equals("PUT") || context.Request.Method.Method.ToUpper().Equals("POST"))
                            {
                                var headerData = ((System.Net.Http.Headers.HttpHeaders)(context.Request.Content.Headers)).Where(x => x.Key == "Content-Type").Select(y => y.Value).FirstOrDefault();
                                if (headerData != null)
                                {
                                    var contentType = (headerData.ToArray()[0]).ToString();
                                    if (context.Request.RequestUri.LocalPath.ToUpper().Contains("CHEQUES"))
                                    {
                                        if (!contentType.ToUpper().Equals("MULTIPART/FORM-DATA"))
                                            ReturnUnsupportedMediaTypeRequest(context);
                                    }
                                    else
                                    {
                                        if (!contentType.ToUpper().Equals("APPLICATION/JSON"))
                                            ReturnUnsupportedMediaTypeRequest(context);
                                    }
                                }
                                else
                                    ReturnUnsupportedMediaTypeRequest(context);
                            }
                        }
                    }
                }
                catch (TokenDecodeException exception)
                {
                    ReturnUnAuthenticated(context);
                }
                catch (ClaimValidationException exception)
                {
                    ReturnUnAuthenticated(context);
                }
            }
        }

        /// <summary>
        /// This method will return the Bad Request
        /// </summary>
        /// <param name="context"></param>
        private void ReturnNotFound(System.Web.Http.Controllers.HttpActionContext context)
        {
            context.Response = new AuthResponseMessage("404", HttpStatusCode.NotFound);
            _isAuthenticated = false;
        }

        /// <summary>
        /// This method will clean up all the AuthTokens
        /// </summary>
        /// <param name="date"></param>
        /// <param name="authRepo"></param>
        private void CleanUpAuthTokens(DateTime date, IAuthManagerRepository authRepo)
        {
            List<string> str = new List<string>();

            foreach (KeyValuePair<string, Token> entry in authRepo.GetAllTokens())
                if ((entry.Value.Expires - date).TotalHours <= 0)
                    str.Add(entry.Key);

            foreach (string s in str)
                authRepo.RemoveAuthToken(s);
            _isAuthenticated = false;
        }




        /// <summary>
        /// This method will return the user is not Authrorized
        /// </summary>
        /// <param name="context"></param>
        public void ReturnUnAthorized(System.Web.Http.Controllers.HttpActionContext context)
        {
            context.Response = new AuthResponseMessage(new AuthErrorResponseMessage("Client is not authorized"), "application/json", "401", HttpStatusCode.Unauthorized);
            _isAuthenticated = false;
        }

        /// <summary>
        /// This method will return the user is not Authrorized
        /// </summary>
        /// <param name="context"></param>
        public void ReturnUnAuthenticated(System.Web.Http.Controllers.HttpActionContext context)
        {
            context.Response = new AuthResponseMessage(new AuthErrorResponseMessage("User is not authenticated"), "application/json", "401", HttpStatusCode.Unauthorized);
            _isAuthenticated = false;
        }

        /// <summary>
        /// The method that ensures that the method returns that the Terms is agreed or not
        /// </summary>
        /// <param name="context"></param>
        public void ReturnTermsNotAgreed(System.Web.Http.Controllers.HttpActionContext context)
        {
            context.Response = new AuthResponseMessage(new AuthErrorResponseMessage("User has not agreed to the terms"), "application/json", "403", HttpStatusCode.Forbidden);
            _isAuthenticated = false;
        }

        /// <summary>
        /// The method that ensures that the method returns false for CCStatus is locked
        /// </summary>
        /// <param name="context"></param>
        public void ReturnStatusLocked(System.Web.Http.Controllers.HttpActionContext context)
        {
            context.Response = new AuthResponseMessage(new AuthErrorResponseMessage("Your account is locked for this services"), "application/json", "403", HttpStatusCode.Forbidden);
            _isAuthenticated = false;
        }

        /// <summary>
        /// This method will return the Bad Request
        /// </summary>
        /// <param name="context"></param>
        private void ReturnUnsupportedMediaTypeRequest(System.Web.Http.Controllers.HttpActionContext context)
        {
            context.Response = new AuthResponseMessage(new AuthErrorResponseMessage("Invalid content type"), "application/json", "415", HttpStatusCode.UnsupportedMediaType);
            _isAuthenticated = false;
        }

        /// <summary>
        /// This method will return if the Auth Key is proper or not
        /// </summary>
        /// <param name="authKey"></param>
        /// <returns></returns>
        private bool CheckAuthKey(string authKey)
        {
            if (authKey.Equals(ConfigurationManager.AppSettings["APIKEY"]) || authKey.Equals(ConfigurationManager.AppSettings["APIKEY2"]) || authKey.Equals(ConfigurationManager.AppSettings["APIKEY3"]) || authKey.Equals(ConfigurationManager.AppSettings["APIKEY4"]) || authKey.Equals(ConfigurationManager.AppSettings["APIKEY5"]))
                return true;
            return false;
        }
    }
}