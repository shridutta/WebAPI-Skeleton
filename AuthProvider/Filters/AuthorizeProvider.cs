﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Filters;
using System.Threading;
using System.Security.Principal;
using System.Text;
using AuthProvider.Models;
using System.Configuration;
using AuthProvider.Repository;
using System.Web.Script.Serialization;
using AuthProvider.ExceptionHandler;
using System.Net;

namespace AuthProvider.Filters
{
    public class AuthorizeProvider : AuthorizationFilterAttribute
    {
        public override void OnAuthorization(System.Web.Http.Controllers.HttpActionContext context)
        {
            // Get the AuthKey and 
            var authKey = context.Request.Headers.Contains("WebAPI-ApiKey") ? Convert.ToString(((string[])(context.Request.Headers.GetValues("WebAPI-ApiKey")))[0]) : String.Empty;

            if (String.IsNullOrEmpty(authKey))
                ReturnUnAthorized(context);
            else if (!CheckAuthKey(authKey))
                ReturnUnAthorized(context);

            if (context.Response == null)
            {
                if (context.Request.Method.Method.ToUpper().Equals("PUT") || context.Request.Method.Method.ToUpper().Equals("POST"))
                {
                    var headerData = ((System.Net.Http.Headers.HttpHeaders)(context.Request.Content.Headers)).Where(x => x.Key == "Content-Type").Select(y => y.Value).FirstOrDefault();
                    if (headerData != null)
                    {
                        var contentType = (headerData.ToArray()[0]).ToString();
                        if (!string.IsNullOrEmpty(contentType))
                        {
                            if (!contentType.ToUpper().Equals("APPLICATION/JSON"))
                                ReturnUnsupportedMediaTypeRequest(context);
                        }
                    }
                    else
                        ReturnUnsupportedMediaTypeRequest(context);
                }
            }
        }

        /// <summary>
        /// This method will return the Bad Request
        /// </summary>
        /// <param name="context"></param>
        private void ReturnNotFound(System.Web.Http.Controllers.HttpActionContext context)
        {
            context.Response = new AuthResponseMessage("404", HttpStatusCode.NotFound);
        }

        /// <summary>
        /// This method will return the Bad Request
        /// </summary>
        /// <param name="context"></param>
        private void ReturnUnsupportedMediaTypeRequest(System.Web.Http.Controllers.HttpActionContext context)
        {
            context.Response = new AuthResponseMessage(new AuthErrorResponseMessage("Invalid content type"), "application/json", "415", HttpStatusCode.UnsupportedMediaType);
        }


        /// </summary>
        /// <param name="context"></param>
        private static void ReturnUnAthorized(System.Web.Http.Controllers.HttpActionContext context)
        {
            context.Response = new AuthResponseMessage(new AuthErrorResponseMessage("Client is not authorized"), "application/json", "401", HttpStatusCode.Unauthorized);
        }


        /// <summary>
        /// This method will return if the Auth Key is proper or not
        /// </summary>
        /// <param name="authKey"></param>
        /// <returns></returns>
        private bool CheckAuthKey(string authKey)
        {
            if (authKey.Equals(ConfigurationManager.AppSettings["APIKEY"]) || authKey.Equals(ConfigurationManager.AppSettings["APIKEY2"]) || authKey.Equals(ConfigurationManager.AppSettings["APIKEY3"]) || authKey.Equals(ConfigurationManager.AppSettings["APIKEY4"]) || authKey.Equals(ConfigurationManager.AppSettings["APIKEY5"]))
                return true;
            return false;
        }
    }
}